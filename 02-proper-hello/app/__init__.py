import os
from typing import Optional
from flask import Flask


def create_app(test_config: Optional[dict] = None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    try: # ensure that the instance folder exists
        os.makedirs(app.instance_path)
    except OSError:
        pass

    with app.app_context():
        from . import routes
        return app
    