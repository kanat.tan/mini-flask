from flask import current_app as app
from flask import (render_template, request)

@app.route('/hello')
def say_hello():
    name = request.args.get('name', 'World')
    return render_template(
        'layout.html',
        greet_name=name,
    )