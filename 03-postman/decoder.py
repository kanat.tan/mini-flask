from flask import (Flask, request)
from flask.json import jsonify

app = Flask(__name__)

def pretty_print_html(data: dict):
    items = "\n".join(
        f'<li>{repr(k)}: {repr(v)}' for k, v in data.items()
    )
    return f'<ul>{items}</ul>'

@app.route('/show_headers')
def show_headers():
    headers = dict(request.headers)
    if request.args.get('pretty', 'false') == 'true':
        return pretty_print_html(headers)
    return jsonify(headers)

@app.route('/show_form', methods=['POST'])
def show_form():
    form_data = request.form.to_dict()
    if request.args.get('pretty', 'false') == 'true':
        return pretty_print_html(form_data)
    return jsonify(form_data)

@app.route('/show_json', methods=['POST'])
def show_json():
    json_data = request.get_json()
    if request.args.get('pretty', 'false') == 'true':
        return pretty_print_html(json_data)
    return jsonify(json_data)