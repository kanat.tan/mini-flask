# Flask + SQLAlchemy Backend 

We will build simple web applications using Python Flask, where access to the database will happen through SQLAlchemy. First, we'll set up a "sane" workstation environment. Then, two styles of applications will be discussed:

* Traditional server-side rendering 
* REST-type API (cross reference: client-side UI next time)

Programming in Flask/SQLAlchemy might feel like driving a stick-shift (i.e., semi-automatic). Next week, we'll look at a framework, which favors ease of use and ease of quick prototyping, and hence feels a lot more automatic (at the cost of appearing like black magic).

## Preparing the Environment: `pipenv`

A Python team's dream:

* Uniform environment across machines/developers/etc.
* Not having to set/control Python version manually
* Not having to install (Python) packages manually

`pipenv` (Python Dev Workflow for Humans) "automatically" creates and manages Python environment (i.e., versioning and packages). How?

* Maintain a `Pipefile` and `Pipfile.lock` 

The rest of this lesson will use a `pipenv`-created environment:

```bash
pipenv --python 3.9 # Create a pipenv in the current folder using Python 3.9
pipenv shell        # Activate your shell to use this environment
```

### Setting up Flask and Friends

[Flask](https://flask.palletsprojects.com/) is a light-weight "micro" web framework for Python. 

The term "micro framework" usually means:

* Simple and extensible
* Small and light-weight core: The core framework has no abstraction layer, no database connectors, no form support, etc.
* But the core framework synergistically works with addons or some other 3rd party packages, forming a rich ecosystem. 

Let's make a new folder and set up `pipenv`, together with Flask, in that folder. The most basic Flask setup:

```bash
pipenv install Flask # No version specified = latest
pipenv graph         # Show the packages we've installed in this environment
```

We can quickly confirm that we've got Flask installed:

```python
>>> import flask
>>> flask.__version__
'2.0.1'
```

## Hello, World

### The Simplest Flask App

It's just a "Hello, World" program. How hard can it be? Below is a simple "Hello, World" program in a single file:

```python
# hello.py

from flask import Flask

app = Flask(__name__)

@app.route('/') # This will intercept GET requests going directly to /
def hello():
    return 'Hello, World!'  # This is what it returns
```

Let's run this app.

```bash
FLASK_APP=hello flask run
```

The default port is `5000`. You can connect directly to it at `http://localhost:5000`.

#### Exercise: Change The Return Message

What we return is really an html page, meaning html tags are possible. Try returning something else as your heart desires. 

### Hello, `<name>`  &mdash; Basic Parameter Passing

Several options exist for clients to send over data to the server, including:

* Query fragment (e.g., `http://awesome.com/hello?name=Alice`)
* Path name (e.g., `http://awesome.com/hello/Alice/say`)
* Encode it as part of the request header (e.g., one of the headers is, say, `Name: Alice`)
* Form data (send a `POST` request with the body storing a "form")
* Other `POST` body (Popular: JSON-formatted data) 

In this demo, we'll aim to 

> Customize our hello message to say "Hello, <name>", where that <name> is an "input" parameter.

#### Idea 1: Query Fragment

We will bring in more stuff via `import`

```python
from flask import Flask, request
```

The `request` object stores everything you ever need to know about the incoming request. Then, we'll simply read off the relevant query parameter.

```python
@app.route('/hello')
def gloried_hello():
    name = request.args.get('name', 'World')
    return f'Hello, {name}!'
```

### Idea 2: Path Name

This is actually a slightly simpler alternative:

```python
@app.route('/hello/<string:name>/say')
def path_encoded_hello(name: str):
    return f'Hello, {name}!'
```

The gist of this approach is the path route parameter. In this case, `/hello/<string:name>/say` says unpack the thing between `/hello/` and `/say`&mdash;and we insist it's a string and call it `name`. This is then passed to the parameter `name: str`. 

### Returning Structured Data

Human-friendly response is great for the eyes. But it's not always the optimal format when the receiving end is a program, expecting to consume and use it for further processing. An important currency in web data exchange is JSON (JavaScript Object Notation) because as the name suggests, it plays nice with JavaScript (which is what's running in most browsers)!

With JSON, we can send structured data (e.g., nested dictionaries or lists) across the inter-web quite easily. As a "contrived" example:

```python
@app.route('/robo/<string:name>')
def robotic_hello(name: str):
    Global.user_count += 1
    res = {
        'greeting': 'Hello',
        'name': name,
        'magic': 42 + len(name),
        'count': Global.user_count,
    }
    return jsonify(res)
```

Demo: We'll access this via a browser and inspect the outcome. 

## Recommend Flask Project Structure 

Other than the simplest projects, our Flask project is expected to span several files&mdash;a good recipe for managing complexity. In this regard, Flask recommends using the factory pattern to create the main application, allowing other components to be added without much fuss to the setup code.  

Take a look at code inside [`02-proper-hello`](02-proper-hello/) for detail. Keep in mind:

* Use `pipenv install` to (re-)create the environment originally
* For version control, both `Pipfile` and `Pipfile.lock` are expected to be part of the repository.
* Use `FLASK_APP=app flask run` to run the server in dev mode.
* This code showcases how a templating engine can be used to help create "prettier" website.

For (even) larger project, Flask has a helper tool called [Flask Blueprint](https://flask.palletsprojects.com/en/2.0.x/blueprints/). You're encouraged to check this out later.

## Postman 

How can we easily send form data and/or JSON-encoded data? 

* Write a program
* Script `curl` to do this (you can, really!)
* Postman (to our rescue)

Postman is `curl` of 2021: modern-looking and a GUI, with in-app "purchase" kind of deal.

The Flask application inside [`03-postman`](03-postman/) shows how to access information sent via different (common) means. It displays the data sent to the server either by sending back JSON-encoded data or rendering the data as HTML (with `pretty=true` query parameter). 

```python
@app.route('/show_headers')
def show_headers():
    headers = dict(request.headers)
    if request.args.get('pretty', 'false') == 'true':
        return pretty_print_html(headers)
    return jsonify(headers)

@app.route('/show_form', methods=['POST'])
def show_form():
    form_data = request.form.to_dict()
    if request.args.get('pretty', 'false') == 'true':
        return pretty_print_html(form_data)
    return jsonify(form_data)

@app.route('/show_json', methods=['POST'])
def show_json():
    json_data = request.get_json()
    if request.args.get('pretty', 'false') == 'true':
        return pretty_print_html(json_data)
    return jsonify(json_data)
```

Our demo will use Postman to send the following requests:

* `GET` request with custom headers, with `pretty=true` and otherwise.
* `POST` request with the body storing form-encoded data, with `pretty=true` and otherwise.
* `POST` requests with the body storing JSON-encoded data, with `pretty=true` and otherwise.

## Connect to a DB and using SQLAlchemy 

### Two Camps of Databases and Two Schools of Philosophy

* There are those who believe that data are organized and structured (i.e., they adhere to some predetermined rules/schemas.) vs. those who think otherwise. This translates roughly to "relational DB" vs. "non-relational DB" (aka. NoSQL DB). More in your DB course, I expect.
* Separately, there are those who believe that one should hand-write the best instructions (often known as queries) for the DB vs. those who think that life is too short and it's okay to sacrifice performance for a bit of sanity.

<u>Today:</u> we'll pretend to be sane people who use relational DB, hence the choice of using an object relational mapping (ORM) to help talk to the DB and using MySQL as our DB backend.

#### Setting Up MySQL the Easy Way(?)

```bash
docker rm -f flask-mysql-db  # delete existing container if any
docker run --name flask-mysql-db \
           -e MYSQL_ROOT_PASSWORD=fl45k \
           -e MYSQL_USER=muic \
           -e MYSQL_PASSWORD=15o2 \
           -e MYSQL_DATABASE=blog \
           -p 3306:3306 \
           -d \
           mysql:8
```

We're setting up MySQL 8, without mapping the volume to an outside location. This means if we shut down this container, our data will be lost. Refer back to the Docker lesson for how to set up volumes to persist data, etc.

### SQLAlchemy Basics

[SQLAlchemy](https://www.sqlalchemy.org/) is an ORM for Python, among many other features it provides. This means it is capable of handling more than simple CRUD "table" operations. For example, it supports enforcing relations and other dependencies. To keep this demo simple, we will operate with a single table, with a simple increment ID as the primary key (PK).

To use this with Flask, we need an extra package:

```bash
pipenv install flask_sqlalchemy PyMySQL cryptography # install other dependencies as well
```

We will create a rudimentary Twitter system, so pretty much the only table necessary (`Message`) only needs these fields:

* `message_id` - a auto-increment unique identifier for the message
* `author` - a string field
* `message` - a string field
* `timestamp` - date/time when was this created

The demo is inside [`04-sql`](04-sql/), where we follow the proper project structure and use the factory pattern. Data models will be kept in `models.py` and hooked into the app creation via `db.init_app`.  The main line declaring the `Message` model looks as follows:

```python
from . import db # db comes from __init__.py

class Message(db.Model):
    """Data model for Messages"""

    __tablename__ = 'message'
    message_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    author = db.Column(db.String(100))
    message = db.Column(db.Text)
    timestamp = db.Column(db.DateTime)
```

## Assignment

### Problem 1

Create a web app that accepts `POST`  requests to `/wc/<name>`, where the body of the request is JSON-encoded data of the following format:

```json
{
    "data": "....."
}
```

The `data` fields a free-flowing string. Your goal is to return the following JSON "histogram":

```json
{
    "requester": "<name>",
    "histogram": {
        "a": ...,
        "b": ...,
        "c": ...,
        ...,
        "z": ...,
        "nonletters": ...
    }
}
```

(The precise format doesn't really matter. You will learn to construct nested JSON structures.)

### Problem 2

Say CS wants a face board of all interested CS students. We'll keep the following info about each student:

* Full name
* Preferred name (i.e., how you want people to call you)
* Email
* Short description about the person

Design a model to keep this data, complete with a web application that supports the following:

* Create a new student profile via Postman, preferably via a `POST` JSON-encoded request to, say, `/profiles`. (You can integrate it with front-end code next time)
* List all members by visiting `/profiles` (a `GET` request). This will be shown as a nicely-rendered HTML page. You should learn more about templating and use it.
* List all members by visiting `/profiles?json=true`. This will cause the response to be JSON-encoded.

Optionally, learn about HTML form&mdash;and create a page to let the user edit a user profile, for example, by visiting `/profiles/<email>/edit`. 
