from . import db # db comes from __init__.py

class Message(db.Model):
    """Data model for Messages"""

    __tablename__ = 'message'
    message_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    author = db.Column(db.String(100))
    message = db.Column(db.Text)
    timestamp = db.Column(db.DateTime)